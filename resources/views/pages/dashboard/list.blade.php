@extends('layouts.default')
@section('content')
<form  method="post" action="{{url('api/customer')}}" enctype="multipart/form-data">
    <div class="row no-gutters payment-list survey-management" id="question-create">
        <div class="col-md-12">
            <div class="page-head-wrap">
                <span class="main-title">Customer Desk</span>
            </div>
        </div>
        @if(session()->has('message'))
        <div class="alert alert-success" role="alert">
            {{ session()->get('message') }}
        </div>
        @elseif(session()->has('fail'))
        <div class="alert alert-danger" role="alert">
            {{ session()->get('fail') }}
        </div>
        @endif
        
        <div class="col-md-12 bg-white cust-mar-top body-content">
            <div class="row no-gutters">
                <div class="col-md-8 offset-md-2">
                    <div class="col-md-8 form-group p-0">
                        <label for="exampleFormControlTextarea1">Name</label>
                        <input type="text" class="form-control main-input text-left w-50 mb-2"
                            name="name" placeholder="Enter name">
                    </div>
                    <div class="col-md-8 form-group p-0">
                        <label for="exampleFormControlTextarea1">NIC</label>
                        <input type="text" class="form-control main-input text-left w-50 mb-2"
                            name="nic" placeholder="Enter nic">
                    </div>
                    <div class="col-md-8 form-group p-0">
                        <label for="exampleFormControlTextarea1">Address</label>
                        <input type="text" class="form-control main-input text-left w-50 mb-2"
                            name="address" placeholder="Enter address">
                    </div>
                    <div class="form-group">
                        <label for="">Telphone</label>
                        <input type="text" class="form-control main-input text-left w-50 mb-2"
                            placeholder="Enter phone number" name="tel1">
                        <div class="d-flex flex-row align-items-center" v-for="(input, index) in inputs">
                            <input type="text" class="form-control main-input text-left w-50 mb-2" v-model="input.one"
                                placeholder="Enter phone number">
                        </div>
                    </div>
                    <div class="row no-gutters text-right">
                        <a herf="#" class="btn btn-info addRow">+</a>
                    </div>
                    <!-- <div class="row no-gutters text-left">
                        <a herf="#" class="btn btn-danger">-</a>
                    </div> -->
                    <div>
                        <div class="cre-btn text-right mt-4">
                            <button type="submit" class="main-btn">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
    <script>
    
        let questionCreate = new Vue({
            el: '#question-create',
            data() {
                return {
                    inputs: []
                };
            },
            methods: {
                addInput() {
                    if (this.inputs.length > 7) {
                        alert("You have reached the maximum numbers limit.");
                    } else {
                        this.inputs.push({
                            one: '',
                            two: ''
                        })
                    }

                }
            }
        })
    </script>
@endsection




