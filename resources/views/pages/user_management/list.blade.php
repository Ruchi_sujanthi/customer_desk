@extends('layouts.default')
@section('content')
    <div class="row no-gutters payment-list">
        <div class="col-md-10 offset-md-1">
            <div class="d-flex flex-row align-items-center page-head-wrap">
                <span class="main-title">User list</span>
                <a href="{{route('user-management.create')}}" class="main-btn ml-auto">Create new</a>
            </div>
            @if(session()->has('message'))
            <div class="alert alert-success" role="alert">
                {{ session()->get('message') }}
            </div>
            @elseif(session()->has('fail'))
            <div class="alert alert-danger" role="alert">
                {{ session()->get('fail') }}
            </div>
            @endif
            @if(isset($users))
                <div class="table-content bg-white">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col" class="border-0">Email</th>
                            <th scope="col" class="border-0">Username</th>
                            <th scope="col" class="border-0">Role</th>
                            <th scope="col" class="border-0">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($users as $user)
                        
                        <tr>
                            <th scope="row">{{ $user['email'] }}</th>
                            <th scope="row">{{ $user['name'] }}</th>
                            <td>{{ $user['role'] }}</td>
                            <td>
                                <a class="edit" href="{{url('edit-user/'.$user['id'])}}">Edit</a>
                                <a class="edit mx-3" href="{{url('reset-pwd/'.$user['id'])}}">Reset</a>
                                <form id="delete_user_4" style="display: inline;" method="POST" action="{{url('api/deleteUser')}}">
                                <input type="hidden" name="id" value="{{ $user['id'] }}">
                                    <button
                                        onclick="return showConfirm('warning','Delete','Are you sure ?','Delete', 'delete_user_4')"
                                        type="button"
                                        class="btn btn-danger btn-xs">
                                        <i class="fas fa-trash-alt"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @endif
        </div>
    </div>

@endsection


{{--
<div class="links">
    <a href="https://github.com/laravel/laravel">GitHub</a>
    <a href="api/socialLoginFb">Facebook</a>
    <a href="api/socialLoginGoogle">Google</a>

</div>
--}}
