@extends('layouts.default')
@section('content')
<form method="post" action="{{url('api/CreateUser')}}" enctype="multipart/form-data">
    <div class="row no-gutters payment-list create" id="user-create">
        <div class="col-md-12">
            <div class="page-head-wrap">
                <span class="main-title">Create user</span>
            </div>
        </div>
             @if(session()->has('message'))
            <div class="alert alert-success" role="alert">
                {{ session()->get('message') }}
            </div>
            @elseif(session()->has('fail'))
            <div class="alert alert-danger" role="alert">
                {{ session()->get('fail') }}
            </div>
            @endif
            <div class="col-md-12 bg-white table-content">
                <div class="col-md-6 offset-md-3">
                    <div class="form-group">
                        <label for="">Email</label>
                        <input name="email" type="text" class="form-control main-input" placeholder="Enter Email address">
                    </div>
                    <div class="form-group">
                        <label for="">Username</label>
                        <input name="name" type="text" class="form-control main-input" placeholder="Enter user name">
                    </div>
                    <div class="form-group">
                        <label for="">Password</label>
                        <input name="password" type="password" class="form-control  main-input" placeholder="Enter password">
                    </div>
                    <!-- <div class="features">
                        <label for="">ROLE</label>
                        <div class="custom-control custom-radio mb-2">
                            <input type="radio" id="customRadio1" name="admin" class="custom-control-input" v-model="selected" value="role_1">
                            <label class="custom-control-label ml-3" for="customRadio1" :class="[selected === 'role_1' ? 'add-clr' : '']">Admin</label>
                        </div>
                        <div class="custom-control custom-radio mb-2">
                            <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input" v-model="selected" value="role_2">
                            <label class="custom-control-label ml-3" for="customRadio2" :class="[selected === 'role_2' ? 'add-clr' : '']">Role 2</label>

                        </div>
                        <div class="custom-control custom-radio mb-2">
                            <input type="radio" id="customRadio3" name="customRadio" class="custom-control-input" v-model="selected" value="role_3">
                            <label class="custom-control-label ml-3" for="customRadio3" :class="[selected === 'role_3' ? 'add-clr' : '']">Role 3</label>
                        </div>
                    </div> -->
                    <div>
                        <div class="cre-btn text-right mt-4">
                            <a href="{{ url()->previous() }}" class="main-btn">Back</a>
                            <button type="submit" class="main-btn">Create</button>
                        </div>
                    </div>
                </div>
            </div>

    </div>
    </form>
    <script>
        let userCreate = new Vue({
            el: '#user-create',
            data() {
                return {
                    selected: 'role_1'
                };

            }
        })
    </script>
@endsection




