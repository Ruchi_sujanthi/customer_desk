@extends('layouts.default')
@section('content')
    <div class="row no-gutters payment-list">
        <div class="col-md-10 offset-md-1">
            <div class="d-flex flex-row align-items-center page-head-wrap">
                <span class="main-title">Customer Report</span>
            </div>
            @if(session()->has('message'))
            <div class="alert alert-success" role="alert">
                {{ session()->get('message') }}
            </div>
            @elseif(session()->has('fail'))
            <div class="alert alert-danger" role="alert">
                {{ session()->get('fail') }}
            </div>
            @endif
            @if(isset($customers))
                <div class="table-content bg-white">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col" class="border-0">Name</th>
                            <th scope="col" class="border-0">NIC</th>
                            <th scope="col" class="border-0">Address</th>
                            <th scope="col" class="border-0">Mobile</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($customers as $cus)
                        <tr>
                            <th scope="row">{{ $cus['name'] }}</th>
                            <th scope="row">{{ $cus['nic'] }}</th>
                            <th scope="row">{{ $cus['address'] }}</th>
                            <th scope="row">{{ $cus['mobile'] }}</th>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @endif
        </div>
    </div>

@endsection


{{--
<div class="links">
    <a href="https://github.com/laravel/laravel">GitHub</a>
    <a href="api/socialLoginFb">Facebook</a>
    <a href="api/socialLoginGoogle">Google</a>

</div>
--}}
