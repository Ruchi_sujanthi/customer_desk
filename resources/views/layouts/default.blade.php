<!doctype html>
<html lang="en">

<head>
    @include('includes.head')
</head>
<body>
<div id="app">
    <div>
        @include('includes.header')
    </div>

    <div class="row no-gutters cust-height">
        <div class="col-md-2 col-12">
            @include('includes.sidebar')
        </div>
        <div class="col-md-10">
            <div class="row no-gutters">
                <div class="col-md-10 offset-md-1 col-sm-10 offset-sm-1">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
    <footer class="w-100">
        @include('includes.footer')
    </footer>
</div>

</body>
</html>

