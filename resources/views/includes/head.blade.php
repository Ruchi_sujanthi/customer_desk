<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="csrf-token">

{{-- Favicon --}}
<link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon">
<link rel="icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon">


<link rel="stylesheet" href="{{ asset('css/app.css') }}">
{{--sweetalert2--}}
<link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css') }}" >
<script src="{{ asset('js/sweetalert2.min.js') }}"></script>

<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>

<script src="{{ asset('js/app.js') }}"></script>
<script>
    function showAlert(type,title,text) {

      Swal.fire({
        title: title,
        text: text,
        type: type,
        confirmButtonText: 'OK'
      });
    }

    function showConfirm(type,title,text,confirmButtonText,formId) {
        console.log("ruchini nneee",formId);
      Swal.fire({
        title: title,
        text: text,
        showCancelButton: true,
        confirmButtonText: confirmButtonText,
        showLoaderOnConfirm: true,
        customClass:{confirmButton: 'btn btn-danger',}
      }).then((result) => {
        
        if (result.value) {
            console.log("ruchinisss",formId);
          $("#delete_user_4").submit();
          return true;
        }
      });

    }
</script>
