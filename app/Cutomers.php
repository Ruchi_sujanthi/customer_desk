<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cutomers extends Model
{
    
    protected $fillable = [
        'cus_id', 'name', 'nic'
    ];
}
