<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_role' => 'required',
            'name' => 'sometimes|nullable|string|max:255',
            'email' => 'required|email|max:255|unique:users,email',
            'nickname' => 'sometimes|nullable|string|max:255|unique:users',
            'password' => 'required',
            'mobile' => 'required|string',
            'birthday' => 'required|date',
            'gender' => 'required|string',
            'address' => 'required|string',
            'avatar' => 'sometimes|nullable|image|mimes:jpg,png,jpeg|max:3500',
            'anonymous' => 'required|boolean',
            
        ];
        
    }

    public function messages()
    {
        return [
            'user_role.required'=>'User Role is required.',
            'email.required'=>'Email is required.',
            'mobile.required'=>'Mobile is required.',
            'birthday.required'=>'Birthday is required.',
            'gender.required'=>'Gender is required.',
            'address.required'=>'Address is required.',
            'avatar.mimes'=>'image only jpeg,jpg,png.',
            'avatar.max'=>'image is must be 3MB.',


        ];
    }
}
