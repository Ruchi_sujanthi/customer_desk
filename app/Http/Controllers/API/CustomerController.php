<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\Helpers\APIHelper;
use App\User;
use App\Cutomers;
use App\Address;
use App\Contacts;

class CustomerController extends Controller
{
    //get Customer list
    Public function GetCustomerList(Request $request) {
        $data = [];
        $successStatus = 200;
        try{
            $customers = Cutomers::orderBy('amount', 'asc')->get();
            foreach($customers as $customer) {
                $res[] = $customer;
            }
            array_push($data,$res);
            
            return redirect()->back()->with('message', 'Successfully updated.');

        } catch (Exception $e) {
            return redirect()->back()->with('message', 'Opps! something went wrong.');
        }
        
    }

    //get Customer list
    Public function AddCustomer(Request $request) {
        $data = [];
        $successStatus = 200;
        try{
            $input = $request->all();
            if(empty($input)){
                return response()->json(APIHelper::createAPIResponse("array could not empty", 201, 'success'), 
                $successStatus);
            }     
            //validation
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'nic' => 'required|regex:/^\d{9}V$/',
                'tel1'=> 'required|regex:/(01)[0-9]{9}/'
            ]);

            if ($validator->fails()) {
                return redirect()->back()->with('message', $validator->errors()); 
            }
            $customer = Cutomers::create($input);

            $contacts = array(
                "customer_id" => $customer->id,
                "contact_no" => $input['tel1'],
            );

            $contacts = Contacts::create($contacts);

            $address = array(
                "customer_id" => $customer->id,
                "address" => $input['address'],
            );
            $address = Address::create($address);

            return redirect()->back()->with('message', 'Successfully updated.');

        } catch (Exception $e) {
            return redirect()->back()->with('message', 'Opps! something went wrong.');
        }
        
    }


    public function deleteCustomer(Request $request)
    {
        $data = [];
        $successStatus = 200;
        try{
            if (Cutomers::where('cus_id', $request->input('id'))->delete()) {
                return redirect()->back()->with('message', 'Successfully updated.');
            } else {
                return redirect()->back()->with('message', 'Opps! something went wrong.');
            }

            return redirect()->back()->with('message', 'Successfully updated.');

        } catch (Exception $e) {
            return redirect()->back()->with('message', 'Opps! something went wrong.');
        }
    }


    public function updateCustomer(Request $request)
    {

        try{
            
            $user_de = Cutomers::find($request->input('id'));

            if($request->input('name') != $user_de->name) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required|string'
                ]);
                $user_de->name = $request->input('name');
                if ($validator->fails()) {
                    return redirect()->back()->with('message', $validator->errors()); 
                }
            }
            if($request->input('nic') != $user_de->nic) {
                $validator = Validator::make($request->all(), [
                    'nic' => 'required|string|nic'
                ]);
                $user_de->nic = $request->input('nic');
                if ($validator->fails()) {
                    return redirect()->back()->with('message', $validator->errors()); 
                }
                
            }

            if($request->input('address') != $user_de->address) {
                $user_de->address = $request->input('address');
                
            }

            if($request->input('address') != $user_de->address) {
                $user_de->address = $request->input('address');
                
            }
            
            $user_de->save(); 

            return redirect(url('/user-management'));

        } catch (Exception $e) {
            return redirect()->back()->with('message', 'Opps! something went wrong.');
        }
            
       
    }

}
