<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Employee;
use Auth;
use App\Helpers\APIHelper;

class AdminController extends Controller
{   


    public function logout(Request $request) {
        \Auth::logout();
        return redirect(url('/'));
    }

    public function AdminLogin(Request $request)
    {

        if(Auth::attempt(['username' => $request->input('username'), 'password' => $request->input('password')])) {
            
            $user_verify = User::where(['username' => $request->input('username')])->first();
           
            $user = Auth::user();
            $success['token'] = $user->createToken('customer_desk')->accessToken;
            $success['name'] = $user->name;
            return redirect(url('/dashboard'));
            
        } else {
        
            return redirect()->back()->with('fail', 'Incorrect username or password, Login failed!'); 
        }  

    }



    
}