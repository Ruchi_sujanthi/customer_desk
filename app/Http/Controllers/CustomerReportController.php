<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Plans;
use Validator;
use App\User;
use Auth;
use App\Helpers\APIHelper;
use App\Cutomers;
use App\Address;
use App\Contacts;

class CustomerReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        if($user = Auth::user()) {
            $customers = Cutomers::orderBy('cus_id', 'asc')->get();
            foreach($customers as $cus) {
                $res = [];
                $res['id'] = $cus->cus_id;
                $res['name'] = $cus->name;
                $res['nic'] = $cus->nic;
                $address = Address::where(['customer_id' =>  $cus->cus_id])->first();
                $res['address'] = $address->address;
                $contacts = Contacts::where(['customer_id' =>  $cus->cus_id])->first();
                $res['mobile'] = $contacts->contact_no;
                array_push($data,$res);
            }
            $result = ['customers'  => $data];
            return view('pages.reports.list',$result);
        } else {
            return view('pages.login');
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
