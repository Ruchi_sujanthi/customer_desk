<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mails;
use App\User;

class SendReminderEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $client_profile;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($client_profile)
    {
        $this->$client_profile = $client_profile;
        
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $users = User::where(['user_role' => 2, 'verified' => 1])->get();
        foreach($users as $user) {
            $name = $user->name;
            $mail = $user->email;
            $client_profile = "https://online-theropy-member.arimac.digital/";
            $data = array("name" => $name, "body" => "New Client has registerd. Please check now.\n\n".$client_profile);
            $subject = "New client";
            $send_mail = Mails::sendMail($name,$mail,$subject,$data);
            if($send_mail) {
                $result[] = ['mail' => $mail , 'sent_time' => date('Y-m-d H:i:s')];
            } 
        }
        return $result;
    }
}
