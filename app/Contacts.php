<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id', 'contact_no'
    ];
}
