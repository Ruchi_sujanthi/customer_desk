<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('AdminLogin', 'API\AdminController@AdminLogin');
Route::get('logout', 'API\AdminController@logout');

//API to get customer data
Route::get('customer', 'API\CustomerController@GetCustomerList');
//API to create customer
Route::post('customer', 'API\CustomerController@AddCustomer');
//API to delete customer data
Route::delete('customer', 'API\CustomerController@deleteCustomer');
//API to update customer
Route::put('customer', 'API\CustomerController@updateCustomer');

